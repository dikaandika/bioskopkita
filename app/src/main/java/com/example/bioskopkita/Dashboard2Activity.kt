package com.example.bioskopkita

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.btnLogout
import kotlinx.android.synthetic.main.activity_dashboard2.*

class Dashboard2Activity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogout -> {
                var intent = Intent (this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btnBioskop->{
                var intent = Intent (this, Data2Activity::class.java)
                startActivity(intent)
            }
            R.id.btnRute->{
                var intent = Intent (this, MapActivity::class.java)
                startActivity(intent)
            }
            R.id.btnAbout->{
                var intent = Intent (this, AboutActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard2)
        btnBioskop.setOnClickListener(this)
        btnRute.setOnClickListener(this)
        btnAbout.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
    }
}