package com.example.bioskopkita

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_data.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class DataActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btImg -> {
                fileType = ".jpg"
                intent.setType("image/*")
            }
            R.id.btUpload -> {
                if(uri != null){
                    fileNama = edNama.text.toString()
                    val fileRef = storage.child(fileNama+fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { task ->
                            val hm = HashMap<String, Any>()
                            hm.put(F_NAMA, fileNama)
                            hm.put(F_ID, edId.text.toString())
                            hm.put(F_ALAMAT, edAlamat.text.toString())
                            hm.put(F_TYPE, fileType)
                            hm.put(F_URL, task.result.toString())
                            db.document(fileNama).set(hm).addOnSuccessListener {
                                Toast.makeText(
                                    this,
                                    "file successfully uploaded",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                }
            }
        }
        if(v?.id != R.id.btUpload) startActivityForResult(intent, RC_OK)

    }

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter : ShowActivity
    lateinit var uri : Uri
    val F_NAMA = "file_nama"
    val F_ALAMAT = "file_alamat"
    val F_ID = "file_id"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val RC_OK = 100
    var fileType =""
    var fileNama =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data)

        btImg.setOnClickListener(this)
        btUpload.setOnClickListener(this)

        alFile = ArrayList()
        uri = Uri.EMPTY
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("files")
        db.addSnapshotListener { querySnapshot, firebaseFirestoreException ->
            if(firebaseFirestoreException != null){
                Log.e("firestore :", firebaseFirestoreException.message.toString())
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for(doc in result){
                val hm = HashMap<String,Any>()
                hm.put(F_NAMA, doc.get(F_NAMA).toString())
                hm.put(F_TYPE, doc.get(F_TYPE).toString())
                hm.put(F_ID, doc.get(F_ID).toString())
                hm.put(F_ALAMAT, doc.get(F_ALAMAT).toString())
                hm.put(F_URL, doc.get(F_URL).toString())
                alFile.add(hm)
            }
            adapter = ShowActivity(this,alFile)
            lsV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null){
                uri = data.data!!
                txSelectedFile.setText(uri.toString())
            }
        }
    }
}