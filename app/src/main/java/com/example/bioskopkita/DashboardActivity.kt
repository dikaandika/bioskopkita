package com.example.bioskopkita

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnLogout -> {
                var intent = Intent (this, LoginActivity::class.java)
                startActivity(intent)
            }
            R.id.btnData->{
                var intent = Intent (this, DataActivity::class.java)
                startActivity(intent)
            }
            R.id.btnAbout->{
                var intent = Intent (this, AboutActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        btnData.setOnClickListener(this)
        btnAbout.setOnClickListener(this)
        btnLogout.setOnClickListener(this)
    }
}