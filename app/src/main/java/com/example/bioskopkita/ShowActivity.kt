package com.example.bioskopkita

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso

class ShowActivity(val context : Context, arrayList : ArrayList<HashMap<String, Any>>) : BaseAdapter() {

    val F_ID = "file_id"
    val F_NAMA = "file_nama"
    val F_ALAMAT = "file_alamat"
    val F_TYPE = "file_type"
    val F_URL = "file_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txId : TextView? = null
        var txNama : TextView? = null
        var txAlamat : TextView? = null
        var txFileType : TextView? = null
        var txFileUrl : TextView? = null
        var imv : ImageView? = null
    }
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view = convertView
        if (convertView == null) {
            var inflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.activity_showdata, null, true)
            holder.txId = view!!.findViewById(R.id.txId) as TextView
            holder.txNama = view!!.findViewById(R.id.txNama) as TextView
            holder.txAlamat = view!!.findViewById(R.id.txAlamat) as TextView
            holder.imv = view!!.findViewById(R.id.imv) as ImageView

            view.tag = holder
        } else {
            holder = view!!.tag as ViewHolder
        }

        var fileType = list.get(position).get(F_TYPE).toString()
        uri = Uri.parse(list.get(position).get(F_URL).toString())

        holder.txId!!.setText(list.get(position).get(F_ID).toString())
        holder.txNama!!.setText(list.get(position).get(F_NAMA).toString())
        holder.txAlamat!!.setText(list.get(position).get(F_ALAMAT).toString())


        when(fileType){
            ".jpg" ->   {
                Picasso.get().load(uri).into(holder.imv)}
        }

        return view!!
    }

    override fun getItem(position: Int): Any {
        return list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }

}